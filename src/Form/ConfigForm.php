<?php
namespace ItemDuplicator\Form;

use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\Color;
use Laminas\Form\Form;

class ConfigForm extends Form
{
    public function init()
    {
        $this->add([
            'name' => 'item_duplicator_restricted',
            'type' => Checkbox::class,
            'options' => [
                'label' => 'If checked, only users with Super User or Admin role will be able to duplicate Items.', // @translate
            ],
        ]);
        $this->add([
            'name' => 'item_duplicator_empty_title',
            'type' => Checkbox::class,
            'options' => [
                'label' => 'If checked, field DublinCore:Title will be emptied when showing duplicate Item..', // @translate
            ],
        ]);
        $this->add([
            'name' => 'item_duplicator_empty_subject',
            'type' => Checkbox::class,
            'options' => [
                'label' => 'If checked, field DublinCore:Subject will be emptied when showing duplicate Item.', // @translate
            ],
        ]);
        $this->add([
            'name' => 'item_duplicator_empty_date',
            'type' => Checkbox::class,
            'options' => [
                'label' => 'If checked, field DublinCore:Date will be emptied when showing duplicate Item.', // @translate
            ],
        ]);
        $this->add([
            'name' => 'item_duplicator_empty_fields_check',
            'type' => Checkbox::class,
            'options' => [
                'label' => 'If checked, fields above are checked before saving and, if any is found empty, a warning will be shown and the saving process will be interrupted.', // @translate
            ],
        ]);
        $this->add([
            'name' => 'item_duplicator_empty_fields_highlight',
            'type' => Color::class,
            'options' => [
                'label' => 'Color hex code (e.g.: #ff0000) to highlight the fields that have been emptied (blank means no highlight).', // @translate
            ],
        ]);
        $this->add([
            'name' => 'item_duplicator_empty_tags',
            'type' => Checkbox::class,
            'options' => [
                'label' => 'If checked, tags will be removed when showing duplicate Item.', // @translate
            ],
        ]);
        $this->add([
            'name' => 'item_duplicator_private',
            'type' => Checkbox::class,
            'options' => [
                'label' => 'If checked, duplicate items will not be made automatically public, even if user role allows that.', // @translate
            ],
        ]);
    }
}
