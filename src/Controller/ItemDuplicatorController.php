<?php
namespace ItemDuplicator\Controller;

/**
 * Omeka
 * 
 * @copyright Copyright 2007-2012 Roy Rosenzweig Center for History and New Media
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GPLv3
 */

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Omeka\Form\ResourceForm;
use Omeka\Stdlib\Message;

/**
 * @package Omeka\Controller
 */
class ItemDuplicatorController extends AbstractActionController
{
	/**
	 * Similar to 'edit' action, except this saves record as new.
	 *
	 * Every request to this action must pass a record ID in the 'id' parameter.
	 *
	 */
	public function duplicateAction()
	{
        $form = $this->getForm(ResourceForm::class);
        $form->setAttribute('action', $this->url()->fromRoute(null, [], true));
        $form->setAttribute('enctype', 'multipart/form-data');
        $form->setAttribute('id', 'edit-item');
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $data = $this->mergeValuesJson($data);
            $form->setData($data);
            if ($form->isValid()) {
                $fileData = $this->getRequest()->getFiles()->toArray();
                $response = $this->api($form)->create('items', $data, $fileData);
                if ($response) {
                    $message = new Message('Item successfully created.'); // @translate
                    $this->messenger()->addSuccess($message);
                    return $this->redirect()->toUrl($response->getContent()->adminUrl('edit', false));
                }
            }
            else
            {
                $this->messenger()->addFormErrors($form);
                // FIXME how to construct the 'item' in this case?
            }
        } else {
            $item = $this->api()->read('items', $this->params('id'))->getContent();
        }
        $layout = $this->layout();
        $layout->setTemplate('/layout/layout-admin');

        $view = new ViewModel;
        $view->setVariable('form', $form);
        $view->setVariable('item', $item);
        $view->setVariable('resource', $item);
        $view->setVariable('mediaForms', []);
        $view->setTemplate('/omeka/admin/item/edit');

        return $view;
	}
}
