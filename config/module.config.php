<?php
namespace ItemDuplicator;

return [
    'controllers' => [
        'invokables' => [
            Controller\ItemDuplicatorController::class => Controller\ItemDuplicatorController::class,
        ],
    ],
    'router' => [
        'routes' => [
            'duplicate' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/admin/item/:id/duplicate',
                    'defaults' => [
                        '__NAMESPACE__' => 'ItemDuplicator\Controller',
                        'controller' => 'ItemDuplicatorController',
                        'action' => 'duplicate',
                    ]
                ],
            ],
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => dirname(__DIR__) . '/language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
    'itemduplicator' => [
        'config' => [
		    'item_duplicator_restricted' => '0',
		    'item_duplicator_empty_title' => '1',
		    'item_duplicator_empty_subject' => '1',
		    'item_duplicator_empty_date' => '1',
		    'item_duplicator_empty_fields_check' => '1',
		    'item_duplicator_empty_fields_highlight' => '#FFFF66',
		    'item_duplicator_empty_tags' => '0',
		    'item_duplicator_private' => '1',
	    ],
    ],
];
