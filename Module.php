<?php

namespace ItemDuplicator;

/**
 * @version $Id$
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 * @copyright Daniele Binaghi, 2018-2020
 * @package ItemDuplicator
 */

use ItemDuplicator\Form\ConfigForm;
use Omeka\Module\AbstractModule;
use Laminas\EventManager\SharedEventManagerInterface;
use Laminas\EventManager\Event;
use Laminas\Mvc\Controller\AbstractController;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\View\Renderer\PhpRenderer;
 
class Module extends AbstractModule
{
	protected $_hooks = array(
		'install',
		'uninstall',
		'initialize',
		'config',
		'config_form',
		'define_acl',
		'define_routes',
		'admin_head',
		'before_save_item',
		'admin_items_panel_buttons'
	);
	
	// Define Filters
	protected $_filters = array(
		'emptyTitleField' => array('ElementInput', 'Item', 'Dublin Core', 'Title'),
		'emptySubjectField' => array('ElementInput', 'Item', 'Dublin Core', 'Subject'),
		'emptyDateField' => array('ElementInput', 'Item', 'Dublin Core', 'Date'),
		'svp_suggest_routes'
	);

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        parent::onBootstrap($event);
        $acl = $this->getServiceLocator()->get('Omeka\Acl');
        $settings = $this->getServiceLocator()->get('Omeka\Settings');
        // TODO check that the settings have correctly been read at this point
        $this->addAclRules($acl, $settings);
    }

    public function install(ServiceLocatorInterface $serviceLocator)
    {
        $settings = $serviceLocator->get('Omeka\Settings');
        $this->manageSettings($settings, 'install');
    }

    public function uninstall(ServiceLocatorInterface $serviceLocator)
    {
        $settings = $serviceLocator->get('Omeka\Settings');
        $this->manageSettings($settings, 'uninstall');
    }

    protected function manageSettings($settings, $process, $key = 'config')
    {
        $config = require __DIR__ . '/config/module.config.php';
        $defaultSettings = $config[strtolower(__NAMESPACE__)][$key];
        foreach ($defaultSettings as $name => $value) {
            switch ($process) {
                case 'install':
                    $settings->set($name, $value);
                    break;
                case 'uninstall':
                    $settings->delete($name);
                    break;
            }
        }
    }

	public function handleConfigForm(AbstractController $controller)
	{
        $services = $this->getServiceLocator();
        $config = $services->get('Config');
        $settings = $services->get('Omeka\Settings');

        $params = $controller->getRequest()->getPost();

        $form = $services->get('FormElementManager')->get(ConfigForm::class);
        $form->init();
        $form->setData($params);
        if (!$form->isValid()) {
            $controller->messenger()->addErrors($form->getMessages());
            return false;
        }

        $defaultSettings = $config[strtolower(__NAMESPACE__)]['config'];
        foreach ($params as $name => $value) {
            if (array_key_exists($name, $defaultSettings)) {
                $settings->set($name, $value);
            }
        }
	}
	
	public function getConfigForm(PhpRenderer $renderer)
	{
		$services = $this->getServiceLocator();
        $config = $services->get('Config');
        $settings = $services->get('Omeka\Settings');
        $form = $services->get('FormElementManager')->get(ConfigForm::class);

        $data = [];
        $defaultSettings = $config[strtolower(__NAMESPACE__)]['config'];
        foreach ($defaultSettings as $name => $value) {
            $value = $settings->get($name);
            $data[$name] = $value;
        }

        $form->init();
        $form->setData($data);
        $html = $renderer->formCollection($form);
        return $html;
	}
	
	public function addAclRules($acl, $settings)
	{
		// admins are always capable of duplicating
		$acl->allow('global_admin', Controller\ItemDuplicatorController::class, 'duplicate');

		if ($settings->get('item_duplicator_restricted')) {
			$acl->deny('site_admin', Controller\ItemDuplicatorController::class, 'duplicate');
			$acl->deny('editor', Controller\ItemDuplicatorController::class, 'duplicate');
			$acl->deny('reviewer', Controller\ItemDuplicatorController::class, 'duplicate');
			$acl->deny('author', Controller\ItemDuplicatorController::class, 'duplicate');
		} else {
			$acl->allow('site_admin', Controller\ItemDuplicatorController::class, 'duplicate');
			$acl->allow('editor', Controller\ItemDuplicatorController::class, 'duplicate');
			$acl->allow('reviewer', Controller\ItemDuplicatorController::class, 'duplicate');
			$acl->allow('author', Controller\ItemDuplicatorController::class, 'duplicate');
		}

		$acl->deny('researcher', Controller\ItemDuplicatorController::class, 'duplicate');
	}

	public function browseItems(Event $event)
    {
        print(<<<EOS
        <script>
            document.addEventListener('DOMContentLoaded', function() {
            var items = document.getElementsByClassName('actions');
            for (i=0; i < items.length; i++) {
                var listItems = items[i].children;
                for (ii=0; ii < listItems.length; ii++) {
                    var newLI = listItems[ii].innerHTML;
                    if (newLI.indexOf('admin/item') > 0 && newLI.indexOf('/edit') > 0) {
                        newLI = newLI.replaceAll('edit"', 'duplicate"');
                        newLI = newLI.replaceAll('Edit"', 'Duplicate"'); // @translate
                        // No icon by default, though you can add one with CSS:
                        newLI = newLI.replace('"></a>', '">[duplicate]</a>'); // @translate
                        var entry = document.createElement('li');
                        entry.innerHTML = newLI;
                        items[i].appendChild(entry);
                        break;
                    }
                }
            }
        }, false);
        </script>
        EOS);
    }
	
	public function attachListeners(SharedEventManagerInterface $sharedEventManager)
	{
        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Item',
            'view.browse.before',
            [$this, 'browseItems'],
        );
        /*
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$controller = $request->getControllerName();
		$action = $request->getActionName();
		if ($controller == 'items') {
			if ($action == 'browse') {			
				queue_js_string("
			");
			} elseif ($action == 'show') {
				queue_js_string("
					document.addEventListener('DOMContentLoaded', function() {
						var panel = document.getElementById('edit');
						var buttons = panel.children;
						for (i=0; i < buttons.length; i++) {
							if (buttons[i].href.indexOf('/items/edit/') > 0) {
								var cln = buttons[i].cloneNode(true);
								cln.innerHTML = '" . __(ITEM_DUPLICATOR_DUPLICATE) . "';
								cln.href = cln.href.replace('items/edit', '" . ITEM_DUPLICATOR_NEWPATH . "');
								buttons[i].parentNode.insertBefore(cln, buttons[i].nextSibling);
								break;
							}
						}
					}, false);
				");
			}
		} elseif (($controller == 'index' && $action == 'index') || ($controller == 'error' && $action == 'not-found')) {
			queue_js_string("
				document.addEventListener('DOMContentLoaded', function() {
					var paragraphs = document.getElementsByClassName('dash-edit');
					for (i=0; i < paragraphs.length; i++) {
						var links = paragraphs[i].children;
						for (ii=0; ii < links.length; ii++) {
							if (links[ii].href.indexOf('/items/edit/') > 0) {
								var cln = links[ii].cloneNode(true);
								cln.innerHTML = '" . __(ITEM_DUPLICATOR_DUPLICATE) . "';
								cln.href = cln.href.replace('items/edit', '" . ITEM_DUPLICATOR_NEWPATH . "');
								links[ii].parentNode.insertBefore(cln, links[ii].nextSibling);
								var textNode = document.createTextNode(' · ');
								links[ii].parentNode.insertBefore(textNode, links[ii].nextSibling);
								break;
							}
						}	
					}
				}, false);
			");
		}
        */
	}
	
	public function hookBeforeSaveItem($args)
	{
		if (get_option(item_duplicator_empty_fields_check)) {
			$request = Zend_Controller_Front::getInstance()->getRequest();
			if (is_null($request)) return; // added to avoid conflict with other plugins
			$controller = $request->getControllerName();
			$action = $request->getActionName();
			//runs checks only when duplicating item
			if ($controller == 'items' && $action == 'duplicate') {
				$item = $args['record'];
				$post = $args['post'];
				// if POST is empty, skip the validation, so it doesn't break when saving item in another way
				if (!empty($post)) {
					// one may simply hardcode DC:Title element id, but it's safer for Item Type Metadata elements
					$titleElement = $item->getElement('Dublin Core', 'Title');
					$title = '';
					if (!empty($post['Elements'][$titleElement->id])) {
						foreach ($post['Elements'][$titleElement->id] as $textbox) {
							$title .= trim($textbox['text']);
						}
					}
					if (empty($title)) {
						$item->addError("DC Title", __('DC Title field cannot be empty!'));
					}
					// one may simply hardcode DC:Subject element id, but it's safer for Item Type Metadata elements
					$subjectElement = $item->getElement('Dublin Core', 'Subject');
					$subject = '';
					if (!empty($post['Elements'][$subjectElement->id])) {
						foreach ($post['Elements'][$subjectElement->id] as $textbox) {
							$subject .= trim($textbox['text']);
						}
					}
					if (empty($subject)) {
						$item->addError("DC Subject", __('DC Subject field cannot be empty!'));
					}
					// one may simply hardcode DC:Date element id, but it's safer for Item Type Metadata elements
					$dateElement = $item->getElement('Dublin Core', 'Date');
					$date = '';
					if (!empty($post['Elements'][$dateElement->id])) {
						foreach ($post['Elements'][$dateElement->id] as $textbox) {
							$date .= trim($textbox['text']);
						}
					}
					if (empty($date)) {
						$item->addError("DC Date", __('DC Date field cannot be empty!'));
					}
					//checks whether item MUST be private
					if (get_option(item_duplicator_private)) {
						$item->setPublic(false);
					}
				}
			}
		}
	}
	
	public function emptyTitleField($components, $args)
	{
		if (get_option('item_duplicator_empty_title')) {
			$request = Zend_Controller_Front::getInstance()->getRequest();
			$controller = $request->getControllerName();
			$action = $request->getActionName();
			if ($controller == 'items' && $action == 'duplicate') {
				$components['input'] = get_view()->formTextarea($args['input_name_stem'] . '[text]', '', array(
					'cols' => 50,
					'rows' => 3,
					'autofocus' => 1,
                    // FIXME read the highlight color from the settings
					'style' => 'background-color: #FFFF66'
				));
			}
		}
		return $components;
	}

	public function emptySubjectField($components, $args)
	{
		if (get_option('item_duplicator_empty_subject')) {
			$request = Zend_Controller_Front::getInstance()->getRequest();
			$controller = $request->getControllerName();
			$action = $request->getActionName();
			if ($controller == 'items' && $action == 'duplicate') {
				$components['input'] = get_view()->formTextarea($args['input_name_stem'] . '[text]', '', array(
					'cols' => 50,
					'rows' => 3,
					'autofocus' => 1,
					'style' => 'background-color: ' . ITEM_DUPLICATOR_HIGHLIGHT_COLOR
				));
			}
		}
		return $components;
	}

	public function emptyDateField($components, $args)
	{
		if (get_option('item_duplicator_empty_date')) {
			$request = Zend_Controller_Front::getInstance()->getRequest();
			$controller = $request->getControllerName();
			$action = $request->getActionName();
			if ($controller == 'items' && $action == 'duplicate') {
				$components['input'] = get_view()->formTextarea($args['input_name_stem'] . '[text]', '', array(
					'cols' => 50,
					'rows' => 3,
					'autofocus' => 1,
					'style' => 'background-color: ' . ITEM_DUPLICATOR_HIGHLIGHT_COLOR
				));
			}
		}
		return $components;
	}

	public function hookAdminItemsPanelButtons($args)
	{
		// Add a 'Cancel' button on the admin right button panel. It appears when editing or duplicating an existing
		// item or adding a new item. When editing or duplicating, pressing the Cancel button takes the user back to
		// the Show page for the item. When adding a new item, it takes them to the Dashboard.
		$itemId = $args['record']->id;
		$url = $itemId ? 'items/show/' . $itemId : '.';
		echo '<a href=' . html_escape(admin_url($url)) . ' class="big blue button">' . __('Cancel') . '</a>';
	}
	
	/**
	 * Add a route to a plugin.
     	 *
     	 * @param array $routePlugins Route plugins array.
     	 * @return array Filtered route plugins array.
    	*/
    	public function filterSvpSuggestRoutes($routePlugins)
    	{
        	$routePlugins['itemduplicator'] = array(
            		'module' => 'item-duplicator',
            		'controller' => 'items',
            		'actions' => array('duplicate')
        	);

        	return $routePlugins;
    	}
}
